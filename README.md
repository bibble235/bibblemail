# bibblemail

Docker container for running roundcube + kolab calendar
## Description
This is an example of installing roundcube + kolab calendar on kubernetes.

## Usage
```
kind: Service
apiVersion: v1
metadata:
  name: bibblemail
  labels:
    app: bibblemail
spec:
  selector:
    app: bibblemail
  ports:
  - name: http
    port: 9251
    targetPort: 9251

---

kind: ConfigMap
apiVersion: v1
metadata:
  name: roundcube.config
  labels:
    app: bibblemail
data:
  roundcube.config.php: |
    <?php
    $config = [];
    $config['db_dsnw'] = 'mysql://roundcube_user:roundcube_pass@roundcube_ip/roundcubemail';

    // Log SQL queries
    $config['sql_debug'] = true;

    // Log IMAP conversation
    $config['imap_debug'] = true;

    // Log LDAP conversation
    $config['ldap_debug'] = true;

    // Log SMTP conversation
    $config['smtp_debug'] = true;

    $config['default_host'] = 'imap.example.com';

    $config['smtp_server'] = 'tls://smtp.gmail.com';
    $config['smtp_user'] = 'fakeuser@gmail.com';
    $config['smtp_pass'] = 'fakepass';
 
    $config['support_url'] = '';

    $config['des_key'] = 'fakekey';

    $config['plugins'] = [
        'debug_logger', 
        'emoticons', 
        'enigma', 
        'filesystem_attachments', 
        'http_authentication', 
        'identicon', 
        'show_additional_headers', 
        'vcard_attachments', 
        'zipdownload', 
        'calendar', 
        'newmail_notifier', 
        'libkolab', 
        'libcalendaring'
    ];

  nginx.vh.conf: |
    server {
        listen   9251;
        root     /var/www/html;
        index    index.php;
        charset  utf-8;

        location /roundcube {
            index index.php index.html;
                if (-f $request_filename) {
                    break;
                }
        }

        location ~ /roundcube/(.+)\.php(/|$) {
            set $script $uri;
            if ($uri ~ "/roundcube/(.+\.php)(/|$)") {
                set $script $1;
            }
            fastcgi_pass localhost:9000;
            include fastcgi_params;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            fastcgi_param  SCRIPT_FILENAME  /var/www/html/roundcube/$script;
        }
    }

---

kind: Deployment
apiVersion: apps/v1
metadata:
  name: bibblemail
  labels:
    app: bibblemail
spec:
  selector:
    matchLabels:
      app: bibblemail
  template:
    metadata:
      labels:
        app: bibblemail
        version: 'fpm'
    spec:
      hostNetwork: true
      containers:
        - name: bibblemail
          image: bibble235/bibblemail:0.0.3
          env:
            - name: SHARE_APP
              value: '1'
          volumeMounts:
            - name: config
              subPath: roundcube.config.php
              mountPath: /app/html/roundcube/config/config.inc.php
              readOnly: true
            - name: src
              mountPath: /shared
        - name: nginx
          image: nginx:stable-alpine
          ports:
            - name: http
              containerPort: 9251
          volumeMounts:
            - name: config
              subPath: nginx.vh.conf
              mountPath: /etc/nginx/conf.d/default.conf
              readOnly: true
            - name: src
              mountPath: /var/www
              readOnly: true
      volumes:
        - name: config
          configMap:
            name: roundcube.config
        - name: src
          emptyDir: {}
```
## Support
You can email me at iwiseman@bibble.co.nz and I may reply.

## Authors and acknowledgment
Thanks to https://github.com/instrumentisto for the base image
## License
This is licensed under GPL-3.0-or-later license.
## Project status
This is a project for my own use and to help others
