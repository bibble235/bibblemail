FROM php:8.0.13-fpm-alpine3.14

LABEL author="Iain (Bill) Wiseman"

ARG roundcube_dir=/var/lib/roundcube-1.5.1

# =======================================
# Install packages
# =======================================
ENV S6_KEEP_ENV=1 S6_CMD_WAIT_FOR_SERVICES=1

RUN rm -rf /var/cache/apk/*

RUN apk update \
&& apk upgrade \
#               && update-ca-certificates \
&& apk add \
    libpng libjpeg libzip libpcre2-32 pcre2-dev\
    s6-overlay   \
    nodejs-less nodejs-less-plugin-clean-css \
    php8-intl \
    php8-gd \
    php8-pspell \
    php8-mysqli \
    php8-pdo \ 
    php8-pdo_mysql \
    php8-sockets \
    php8-sodium \ 
    php8-exif

# =======================================
# Install Roundcube
# =======================================
ARG roundcube_ver=1.5.1

RUN apk add --no-cache --virtual \
    less nodejs-less nodejs-less-plugin-clean-css 

RUN rm -rf /tmp/roundcube.tar.gz \
  \
&& curl -fL -o /tmp/roundcube.tar.gz \
         https://github.com/roundcube/roundcubemail/releases/download/${roundcube_ver}/roundcubemail-${roundcube_ver}.tar.gz \
&& tar -xzf /tmp/roundcube.tar.gz -C /tmp/  \
&& mv /tmp/roundcubemail-${roundcube_ver} ${roundcube_dir} \
  \
# Fix syslog
&& sed -i -r 's/^([^\s]{9}log_driver[^\s]{2} =) [^\s]+$/\1 "syslog";/g' \
       ${roundcube_dir}/config/defaults.inc.php 

# =======================================
# Install Calendar
# =======================================
ARG kolab_ver=3.5.7

RUN  rm -rf /tmp/roundcubemail-plugins-kolab \
    \
&& curl -fL -o /tmp/roundcubemail-plugins-kolab.tar.gz \
  https://cgit.kolab.org/roundcubemail-plugins-kolab/snapshot/roundcubemail-plugins-kolab-${kolab_ver}.tar.gz \
&& tar -xzf /tmp/roundcubemail-plugins-kolab.tar.gz -C /tmp/ \
    \
&& cd /tmp/roundcubemail-plugins-kolab-${kolab_ver}/plugins/ \
    \
&& cp -r calendar                     ${roundcube_dir}/plugins/calendar \
&& cp -r calendar/config.inc.php.dist ${roundcube_dir}/plugins/calendar/config.inc.php \
&& cp -r libcalendaring               ${roundcube_dir}/plugins/libcalendaring \
&& cp -r libkolab                     ${roundcube_dir}/plugins/libkolab \
    \
&& lessc --relative-urls -x ${roundcube_dir}/plugins/libkolab/skins/elastic/libkolab.less > ${roundcube_dir}/plugins/libkolab/skins/elastic/libkolab.min.css
# =======================================
# Download Composer
# =======================================
 RUN curl -fL -o /tmp/composer-setup.php \
          https://getcomposer.org/installer \
 && curl -fL -o /tmp/composer-setup.sig \
          https://composer.github.io/installer.sig \
 && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { echo 'Invalid installer' . PHP_EOL; exit(1); }" \
 && php /tmp/composer-setup.php --install-dir=/tmp --filename=composer 

# =======================================
# Configure Composer
# =======================================
COPY composer.json ${roundcube_dir}/composer.json

RUN cd ${roundcube_dir} \
 && (/tmp/composer suggests --list | xargs -i /tmp/composer require {}) \
 && /tmp/composer install --no-dev --optimize-autoloader --no-progress

 # Resolve Roudcube JS dependencies
 RUN ${roundcube_dir}/bin/install-jsdeps.sh \
 # Make default Roudcube configuration log to syslog
 && sed -i -r 's/^([^\s]{9}log_driver[^\s]{2} =) [^\s]+$/\1 "syslog";/g' \
        ${roundcube_dir}/config/defaults.inc.php

# =======================================
# Setup serve directories
# =======================================
RUN mkdir -p /app/html \
&& mv ${roundcube_dir} /app/html/roundcube \
&& cd /app/ \
&& ln -sn /app /app/html \
&& rm -rf /var/www \
&& ln -s /app /var/www \
&& chown -R www-data:www-data  /app
# =======================================
# Cleanup
# =======================================
RUN (find /app/html/roundcube/ -name .travis.yml -type f -prune | \
       while read d; do rm -rf $d; done) \
&& (find /app/html/roundcube/ -name .gitignore -type f -prune | \
       while read d; do rm -rf $d; done) \
&& (find /app/html/roundcube/ -name .git -type d -prune | \
       while read d; do rm -rf $d; done) \
&& rm -rf /var/cache/apk/* \
          /root/.composer \
          /tmp/*

# Install configurations
COPY rootfs /

# Fix executable rights
RUN chmod +x /etc/services.d/*/run  /docker-entrypoint.sh

ENV PHP_OPCACHE_REVALIDATION=0 \
    PHP_OPCACHE_JIT_BUFFER_SIZE=100M \
    SHARE_APP=0

WORKDIR /var/www

ENTRYPOINT ["/init","/docker-entrypoint.sh"]

CMD ["php-fpm"]